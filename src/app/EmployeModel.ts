export class EmployeModel {
    id: number;
    gender: string;
    email?: string;
    phoneNumber: number;
    contactPreference: string;
    dataOfBirth: Date;
    department: string;
    isActive: boolean;
    photoPath?: string;
    name: string;
    type: string;
}
