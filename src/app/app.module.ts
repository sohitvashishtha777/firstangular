import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Inject } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { CompanyModule } from './company/company.module';
import { MyserviceService } from './myservice.service';
import { AddprefixPipe } from './addprefix.pipe';
import { PageNotFoundComponent } from './page-not-found.component';
import { LoginComponent } from './login.component';
import { GenerateControlsComponent } from './Controls/generate-controls/generate-controls.component';
import { AgGridModule } from 'ag-grid-angular';
import { GridModule, PagerModule, PageService, SortService, FilterService, GroupService } from '@syncfusion/ej2-angular-grids';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatButtonModule} from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    AddprefixPipe,
    PageNotFoundComponent,
    LoginComponent,
    GenerateControlsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CompanyModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    GridModule, PagerModule,
    BrowserAnimationsModule,
    BrowserModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule
  ],
  providers: [
    MyserviceService, PageService, SortService, FilterService, GroupService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(@Inject(MyserviceService)myservice){
    console.log(myservice);
    console.log('This is my module');
  }
 }
