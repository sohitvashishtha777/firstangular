import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgModel, Validators, FormArray} from '@angular/forms';
import { EmployeModel } from 'src/app/EmployeModel';
import { PageSettingsModel } from '@syncfusion/ej2-grids';

@Component({
  selector: 'app-generate-controls',
  templateUrl: './generate-controls.component.html',
  styleUrls: ['./generate-controls.component.css']
})
export class GenerateControlsComponent implements OnInit {
  public pageSetting: PageSettingsModel = { pageSize: 2};
  public data: object[] = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
];
  FormGroup = new FormGroup({
    name: new FormControl(),
    type : new FormControl('')
  });
  userForm: FormControl;
  EmployeModel = new EmployeModel();
  frm = [];
  constructor() { }

  ngOnInit(): void {
    this.EmployeModel = new EmployeModel();
    this.frm.push(this.EmployeModel);
  }
  addControls()
  {
    console.log(this.FormGroup.value);
    this.EmployeModel = new EmployeModel();
    this.EmployeModel.id = this.FormGroup.get('name').value;
    this.EmployeModel.name = this.FormGroup.get('name').value;
    this.EmployeModel.type = this.FormGroup.get('type').value;
    this.frm.push(this.EmployeModel);
    this.data = [
      { make: this.EmployeModel.id, model:  this.EmployeModel.name  , price:  this.EmployeModel.type }
  ];
   // alert(this.EmployeModel.type );
    // this.frm.push(this.EmployeModel);
  }

}
