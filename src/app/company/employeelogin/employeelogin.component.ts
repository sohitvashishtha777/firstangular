import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';
import { FormGroup, FormControl, FormBuilder, NgModel, Validators, FormArray} from '@angular/forms';
import { EmployeModel } from 'src/app/EmployeModel';

@Component({
  selector: 'app-employeelogin',
  templateUrl: './employeelogin.component.html',
  styleUrls: ['./employeelogin.component.css'],
  providers: [MyserviceService]
})
export class EmployeeloginComponent implements OnInit {
  Text: string;
  signup: FormGroup;
  gender: string;
  uname: string;
  psw: string;
  firstname: string;
  // tslint:disable-next-line:typedef-whitespace
  // tslint:disable-next-line:variable-name
 constructor(private frmbuilder: FormBuilder, private _myService: MyserviceService)
 {
 }
 ondata(signup: FormGroup)
 {
   console.log(this.signup.controls );
   this.firstname = this.signup.get('uname').value;
   alert(this.firstname);
   alert(this.signup.value);
 }
  ngOnInit(): void {
    // tslint:disable-next-line:no-unused-expression
    const empdetails = new FormArray([
      new FormControl(''),
      new FormControl('Deepak')
    ]);
    console.log(empdetails.value.Validators.minlength('2'));
    console.log(empdetails.status);
    this.signup = new FormGroup({
      gender: new FormControl('select', Validators.required),
      uname: new FormControl('Enter name', Validators.required),
      psw: new FormControl()
    });
    // this.signup.get('uname').valueChanges.subscribe(
    //   name => {
    //     console.log('first name : ' + name);
    //   }
    // );
    this.signup.get('uname').statusChanges.subscribe(
      name => {
        console.log('first name : ' + name);
      }
    );
    // this.signup.valueChanges.subscribe((name: EmployeModel) =>
    //   {
    //     console.log(name.uname);
    //     console.log(name.psw);
    //   }
    // );
    this.Text = this._myService.display();
  }
SetValue()
{
  this.signup.setValue({
    uname: ' sohit',
    gender: 'Male',
    psw: '12345'
  });
}
PatchValue()
{
  this.signup.patchValue({
    uname: ' sohit',
    gender: 'Male',
  });
}
  reset()
  {
    this.signup.reset();
  }



}
