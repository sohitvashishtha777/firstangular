import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeloginComponent } from './employeelogin/employeelogin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [EmployeeloginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CompanyModule { }
