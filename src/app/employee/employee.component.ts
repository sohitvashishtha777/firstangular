import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgModel, Validators, FormArray} from '@angular/forms';
import { EmployeModel } from 'src/app/EmployeModel';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  FormGroup: FormGroup;
  empList: EmployeModel[] = [{
    id: 1,
    name: 'Sohit',
    isActive: true,
    gender: 'male',
    email: 'sohitvashishtha555@gmail.com',
    dataOfBirth: new Date('01/01/1992'),
    department: 'IT',
    photoPath: 'assets/images/avatar2.png',
    type: '',
    phoneNumber: 859511446,
    contactPreference: 'Email'
      },
      {
        id: 2,
        name: 'Monika',
        isActive: true,
        gender: 'Female',
        email: 'monikagutam@gmail.com',
        dataOfBirth: new Date('01/0171998'),
        department: 'IT',
        photoPath: 'assets/images/avatar6.png',
        type: '',
        phoneNumber: 8597456623,
        contactPreference: 'Email'
          },
          {
            id: 3,
            name: 'Robin',
            isActive: false,
            gender: 'male',
            email: 'robinkumar@gmail.com',
            dataOfBirth: new Date('01/01/2008'),
            department: 'Electrical',
            photoPath: 'assets/images/img_avatar.png',
            type: '',
            phoneNumber: 8745512665,
            contactPreference: 'Email'
              }];
  EmployeModel = new EmployeModel();
  frm = [];
  constructor() { }

  ngOnInit(): void {
    this.EmployeModel = new EmployeModel();
    this.frm.push(this.EmployeModel);
  }
  submit()
  {
    console.log(this.frm);
  }
  AddControl()
  {
    this.EmployeModel = new EmployeModel();
    this.frm.push(this.EmployeModel);
  }
  rempveControl(index)
  {
    this.frm.splice(index);
  }

}
