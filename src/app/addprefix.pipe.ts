import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addprefix'
})
export class AddprefixPipe implements PipeTransform {

  transform(value: string, gen: any): string {
    // tslint:disable-next-line:triple-equals
    if (gen == 'M') {
     return 'Mr.' + value;
    }
     else {
     return 'Miss.' + value;
    }
  }

}
