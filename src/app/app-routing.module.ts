import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeComponent} from './employee/employee.component';
import { EmployeeloginComponent } from './company/employeelogin/employeelogin.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { LoginComponent } from './login.component';
import { GenerateControlsComponent } from './Controls/generate-controls/generate-controls.component';



const routes: Routes = [
  {
    path: '', redirectTo: 'empLogin', pathMatch: 'full'
  },
  {
    path: 'Employee', component: EmployeeComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'AddControls', component: GenerateControlsComponent
  },
  {
    path: 'empLogin', component: EmployeeloginComponent
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
