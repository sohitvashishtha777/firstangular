import { Component } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router){}
  title = 'This is my new Project';
  columnDefs = [
    {headerName: 'Make', field: 'make', sort: true, filter: true},
    {headerName: 'Model', field: 'model', sort: true, filter: true },
    {headerName: 'Price', field: 'price', sort: true, filter: true}
];

rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
];
  isvalid = true;
  choose = '';
  newNumber = 0.1223656021984651;
  companyname = 'Royal Datamatics pvt ltd';
  cols = 7;
  students = [{name : 'sohit'}, {name : 'deepak'}, {name : 'madhumita'}, {name : 'monika'}, {name : 'manoj'}];
  employees = [
    {empId : '1', gen : 'M', name : 'Sohit', age : '25' , dob : '01/01/1992'}
  , {empId : '2', gen : 'F', name : 'Monika', age : '23' , dob : '10/03/1995'}
  , {empId : '3', gen : 'M', name : 'Mohit', age : '55' , dob : '03/30/1998'}];
  countrydata = [
     {country: 'India',
                pepole : [{name : 'sohit'}, {name : 'deepak'}, {name : 'madhumita'}
                , {name : 'monika'}, {name : 'manoj'}]
     },
     {country : 'US',
                 pepole : [{name : 'Alex'}, {name : 'Jex'}, {name : 'mili'}
                 , {name : 'jastin'}, {name : 'cristino'}]
     },
     {country : 'Japan',
                pepole : [{name : 'yanchung'}, {name : 'zung'}, {name : 'yang'}
                , {name : 'bros'}, {name : 'martin'}]
     }
    ];
   AddData(): void {
     this.employees = [
       {empId : '1', gen : 'M', name : 'Sohit', age : '25' , dob : '01/01/1992'}
     , {empId : '2', gen : 'F', name : 'Monika', age : '23' , dob : '03/06/1992'}
     , {empId : '3', gen : 'M', name : 'Mohit', age : '55' , dob : '03/05/1990'}
     , {empId : '4', gen : 'F', name : 'Preeti', age : '25' , dob : '02/07/1995'}
     , {empId : '5', gen : 'M', name : 'Deepak', age : '23' , dob : '12/11/1998'}
     , {empId : '6', gen : 'M', name : 'Nadeem', age : '55' , dob : '11/02/2000'}];
   }
   trackbydata(index: number, employeeid: any): string{
return employeeid.empId;
   }

  buttonclick(){
    alert('Hi!!!!');
  }
  changevalue(valid)
  {
    this.isvalid = valid;
  }
  setvalue(drp)
  {
     this.choose = drp.target.value;
  }
  login()
  {
    this.router.navigate(['/login']);
  }
}
